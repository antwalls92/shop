$(document).ready(function () {
    $("#selecciona-imagen-trigger").click(function () {
        $("#image").trigger("click");
    });
    $("#image").change(function () {
        var url = $("#image").val();
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        var input = this;
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#selecciona-imagen-trigger').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
        else {
            $('#selecciona-imagen-trigger').attr('src', '/images/no-image.jpg');
        }
    });
});