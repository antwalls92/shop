﻿var less = require('gulp-less'),
    path = require('path'),
    gulp = require('gulp'),
    gutil = require('gulp-util');

gulp.task('default', ['fonts','less']);

gulp.task('fonts', function () {
    return gulp.src('node_modules/font-awesome/fonts/*')
        .pipe(gulp.dest('public/fonts'))
})

gulp.task('less', function () {
    return gulp.src('public/stylesheets/*.less')
        .pipe(less())
        .pipe(gulp.dest('public/stylesheets/'));
})