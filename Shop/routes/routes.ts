﻿import * as express from "express";
import homeController = require("../controllers/home/HomeController");
import {app, Upload, requireLogin, logout} from "../app"
import {UserController} from "../controllers/user/UserController";
import adminController = require("../controllers/admin/AdminController");
import productController = require("../controllers/product/ProductController")

export function mainrouter() {

    var userController = new UserController();

    app.get('/', homeController.index);
    app.get('/about', homeController.about);
    app.get('/contact', homeController.contact);

    app.get("/admin/users", requireLogin ,userController.users);
    app.get("/admin/users/index", requireLogin ,userController.users);
    app.get("/admin/user/delete/:id", requireLogin , userController.erase);
    app.get("/admin/user/create", requireLogin, userController.createUser);
    app.get("/admin/user/edit/:id", requireLogin, Upload.single('image'), userController.createUser);
    app.post("/admin/user/create", requireLogin  , Upload.single('image'), userController.createUser);

    app.get("/admin", requireLogin , adminController.index);
    app.get("/admin/login" ,userController.login);
    app.post("/admin/login", userController.login);
    app.get("/admin/index", requireLogin, adminController.index);
    app.get("/admin/products/index", requireLogin ,  adminController.index);
    app.get("/admin/product/create", requireLogin , productController.create);
    app.post("/admin/product/save", requireLogin , Upload.single('image'), productController.saveProduct);
    app.get("/admin/product/edit/:id", requireLogin,  Upload.single('image'), productController.edit);
    app.get("/admin/product/delete/:id", requireLogin, Upload.single('image'), productController.erase);
    app.get("/admin/logout", logout);

    
}