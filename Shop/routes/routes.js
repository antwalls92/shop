"use strict";
var homeController = require("../controllers/home/HomeController");
var app_1 = require("../app");
var UserController_1 = require("../controllers/user/UserController");
var adminController = require("../controllers/admin/AdminController");
var productController = require("../controllers/product/ProductController");
function mainrouter() {
    var userController = new UserController_1.UserController();
    app_1.app.get('/', homeController.index);
    app_1.app.get('/about', homeController.about);
    app_1.app.get('/contact', homeController.contact);
    app_1.app.get("/admin/users", app_1.requireLogin, userController.users);
    app_1.app.get("/admin/users/index", app_1.requireLogin, userController.users);
    app_1.app.get("/admin/user/delete/:id", app_1.requireLogin, userController.erase);
    app_1.app.get("/admin/user/create", app_1.requireLogin, userController.createUser);
    app_1.app.get("/admin/user/edit/:id", app_1.requireLogin, app_1.Upload.single('image'), userController.createUser);
    app_1.app.post("/admin/user/create", app_1.requireLogin, app_1.Upload.single('image'), userController.createUser);
    app_1.app.get("/admin", app_1.requireLogin, adminController.index);
    app_1.app.get("/admin/login", userController.login);
    app_1.app.post("/admin/login", userController.login);
    app_1.app.get("/admin/index", app_1.requireLogin, adminController.index);
    app_1.app.get("/admin/products/index", app_1.requireLogin, adminController.index);
    app_1.app.get("/admin/product/create", app_1.requireLogin, productController.create);
    app_1.app.post("/admin/product/save", app_1.requireLogin, app_1.Upload.single('image'), productController.saveProduct);
    app_1.app.get("/admin/product/edit/:id", app_1.requireLogin, app_1.Upload.single('image'), productController.edit);
    app_1.app.get("/admin/product/delete/:id", app_1.requireLogin, app_1.Upload.single('image'), productController.erase);
    app_1.app.get("/admin/logout", app_1.logout);
}
exports.mainrouter = mainrouter;
//# sourceMappingURL=routes.js.map