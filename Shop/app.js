"use strict";
var express = require('express');
var http = require('http');
var path = require('path');
var multer = require('multer');
var Upload = multer({ dest: 'uploads/' });
exports.Upload = Upload;
var routes_1 = require('./routes/routes');
var db = require("./models");
exports.db = db;
var session = require("client-sessions");
var app = express();
exports.app = app;
app.use(session({
    cookieName: "session",
    secret: "random_string_goes_here",
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
}));
app.use(function (req, res, next) {
    if (req.session && req.session.user) {
        db.User.find({
            where: {
                username: req.session.user.username
            }
        }).then(function (user) {
            if (user) {
                req.user = user;
                delete req.user.password;
                req.session.user = user;
                res.locals.user = user;
            }
            next();
        });
    }
    else {
        next();
    }
});
function requireLogin(req, res, next) {
    if (!req.user) {
        res.redirect("/admin/login");
    }
    else {
        next();
    }
}
exports.requireLogin = requireLogin;
;
function logout(req, res) {
    req.session.reset();
    res.redirect("/");
}
exports.logout = logout;
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
var stylus = require('stylus');
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.locals.basedir = path.join(__dirname, 'views');
if ('development' === app.get('env')) {
    app.use(express.errorHandler());
}
routes_1.mainrouter();
http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
//# sourceMappingURL=app.js.map