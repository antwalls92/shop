"use strict";
var productController = require("../../controllers/product/ProductController");
function index(req, res) {
    productController.getAll()
        .then(function (products) {
        res.render('admin/index', {
            title: 'Viva el Carro',
            year: new Date().getFullYear(),
            products: products
        });
    });
}
exports.index = index;
//# sourceMappingURL=AdminController.js.map