﻿import {db} from "../../app"
import * as express from "express";
import userController = require("../../controllers/user/UserController");
import productController = require("../../controllers/product/ProductController");


export function index(req: express.Request, res: express.Response) {

    productController.getAll()
        .then(function (products) {
            res.render('admin/index', {
                title: 'Viva el Carro',
                year: new Date().getFullYear(),
                products: products
                })
        });
}






