﻿import * as express from "express";
import {upload_file} from "../file/FileController"

export class UserController {

    db: any;
    express: any;
    uploadFile: any;
    bcrypt: any;

    constructor() {
        this.db = require('../../models');
        this.uploadFile = upload_file;
        this.bcrypt = require('bcryptjs');
    }

    public create = (req, res) => {
        var newUser = {
            username: req.body.username,
            password: this.bcrypt.hashSync(req.body.password),
            role: req.body.role,
            id: req.body.id
        };
        var saveAndRedirectAdminhome = newUser => {
            this.db.User.upsert(newUser)
                .then(() => {
                    res.redirect("/admin/users/index");
                });
        };
        this.uploadFile(req,
            res,
            "/images/",
            {
                success() {
                    newUser["photo"] = "/images/" + req.file.originalname;
                    saveAndRedirectAdminhome(newUser);
                },
                error() {
                    saveAndRedirectAdminhome(newUser);
                }
            });
    }

    public exists = (req, res) => {
        var username = req.params.username;
        this.db.User.where(
            { username: username }
        )
            .then(user => {
                if (user) {
                    return true;
                }
                return false;
            });
    }

    public erase = (req, res) => {
        var id = req.params.id;
        this.db.User.findById(id)
            .then(user => {
                user.destroy()
                    .then(() => {
                        res.redirect('/admin/users/index');
                    });
            });
    }

    public get = (req, res) => {
        var id = req.params.id;
        return this.db.User.findById(id);

    }

    public validate = (req: express.Request, callbacks) => {

        var username = req.body.username;
        var password = this.bcrypt.hashSync(req.body.password);

        this.db.User.find({
            where:
            {
                username: username
            }
        })
            .then(user => {
                if (user) {
                    if (password, user.password && username === user.username) {
                        req.session.user = user;
                        callbacks.success();
                    } else {
                        callbacks.error();
                    }

                    callbacks.error();
                }
            });
    }

    public update = (req: express.Request, res: express.Response) => {
        const userloggedid = req.body.loggeduserid;
        var userToChangeid = req.body.id;
        this.db.User.findById(userloggedid)
            .then(loggeduser => {
                if (loggeduser.role === 1) {
                    this.db.User.findById(userToChangeid)
                        .then(userToChange => {
                            this.create(req, res);
                        });
                }
            });
    }

    public allUsers = () => {
        return this.db.User.findAll();
    }

    public allRoles = () => {
        return this.db.Rol.findAll();
    }

    public users = ((req, res) => {

        this.allUsers()
            .then(users => {
                res.render("user/index",
                    {
                        title: "Viva el Carro",
                        users: users
                    });
            });
    });

    public createUser = (req, res) => {

        var getAllRolesAndRedirect = (user): void => {
            this.allRoles()
                .then(roles => {
                    res.render("user/create",
                        {
                            roles: roles,
                            user: user
                        });
                });
        };

         if (req.method === "POST") {
            this.create(req, res);
        } else if (req.method === "GET") {
            if (req.params.id) {
                this.get(req, res)
                    .then((user): void => {
                        getAllRolesAndRedirect(user);
                    });
            } else {
                getAllRolesAndRedirect(null);
            }
        }
    }

    public login = ((req: express.Request, res: express.Response) => {
        if (req.method === "POST") {
            this.validate(req,
                {
                    success() {
                        res.redirect("/admin/index");
                    },
                    error() {
                        res.redirect("admin/index");
                    }
                });

        } else {
            res.render("user/login", {});
        }
    });

}
