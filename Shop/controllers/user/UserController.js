"use strict";
var FileController_1 = require("../file/FileController");
var UserController = (function () {
    function UserController() {
        var _this = this;
        this.create = function (req, res) {
            var newUser = {
                username: req.body.username,
                password: _this.bcrypt.hashSync(req.body.password),
                role: req.body.role,
                id: req.body.id
            };
            var saveAndRedirectAdminhome = function (newUser) {
                _this.db.User.upsert(newUser)
                    .then(function () {
                    res.redirect("/admin/users/index");
                });
            };
            _this.uploadFile(req, res, "/images/", {
                success: function () {
                    newUser["photo"] = "/images/" + req.file.originalname;
                    saveAndRedirectAdminhome(newUser);
                },
                error: function () {
                    saveAndRedirectAdminhome(newUser);
                }
            });
        };
        this.exists = function (req, res) {
            var username = req.params.username;
            _this.db.User.where({ username: username })
                .then(function (user) {
                if (user) {
                    return true;
                }
                return false;
            });
        };
        this.erase = function (req, res) {
            var id = req.params.id;
            _this.db.User.findById(id)
                .then(function (user) {
                user.destroy()
                    .then(function () {
                    res.redirect('/admin/users/index');
                });
            });
        };
        this.get = function (req, res) {
            var id = req.params.id;
            return _this.db.User.findById(id);
        };
        this.validate = function (req, callbacks) {
            var username = req.body.username;
            var password = _this.bcrypt.hashSync(req.body.password);
            _this.db.User.find({
                where: {
                    username: username
                }
            })
                .then(function (user) {
                if (user) {
                    if (password, user.password && username === user.username) {
                        req.session.user = user;
                        callbacks.success();
                    }
                    else {
                        callbacks.error();
                    }
                    callbacks.error();
                }
            });
        };
        this.update = function (req, res) {
            var userloggedid = req.body.loggeduserid;
            var userToChangeid = req.body.id;
            _this.db.User.findById(userloggedid)
                .then(function (loggeduser) {
                if (loggeduser.role === 1) {
                    _this.db.User.findById(userToChangeid)
                        .then(function (userToChange) {
                        _this.create(req, res);
                    });
                }
            });
        };
        this.allUsers = function () {
            return _this.db.User.findAll();
        };
        this.allRoles = function () {
            return _this.db.Rol.findAll();
        };
        this.users = (function (req, res) {
            _this.allUsers()
                .then(function (users) {
                res.render("user/index", {
                    title: "Viva el Carro",
                    users: users
                });
            });
        });
        this.createUser = function (req, res) {
            var getAllRolesAndRedirect = function (user) {
                _this.allRoles()
                    .then(function (roles) {
                    res.render("user/create", {
                        roles: roles,
                        user: user
                    });
                });
            };
            if (req.method === "POST") {
                _this.create(req, res);
            }
            else if (req.method === "GET") {
                if (req.params.id) {
                    _this.get(req, res)
                        .then(function (user) {
                        getAllRolesAndRedirect(user);
                    });
                }
                else {
                    getAllRolesAndRedirect(null);
                }
            }
        };
        this.login = (function (req, res) {
            if (req.method === "POST") {
                _this.validate(req, {
                    success: function () {
                        res.redirect("/admin/index");
                    },
                    error: function () {
                        res.redirect("admin/index");
                    }
                });
            }
            else {
                res.render("user/login", {});
            }
        });
        this.db = require('../../models');
        this.uploadFile = FileController_1.upload_file;
        this.bcrypt = require('bcryptjs');
    }
    return UserController;
}());
exports.UserController = UserController;
//# sourceMappingURL=UserController.js.map