﻿import {db} from "../../app"
import * as express from "express";

export function index(req, res) {
   db.Product.findAll(
        {
            where: {
                id: {
                    $ne: null
                }
            }
        }).then(function (products) {
            res.render('public/index', {
                title: 'Viva el Carro',
                year: new Date().getFullYear(),
                products: products
            });
        });
}


export function about(req, res) {
    res.render('public/about', { title: 'About', year: new Date().getFullYear(), message: 'Your application description page' });
}
export function contact(req, res) {
    res.render('public/contact', { title: 'Contact', year: new Date().getFullYear(), message: 'Your contact page' });
}