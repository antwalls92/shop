"use strict";
var app_1 = require("../../app");
function index(req, res) {
    app_1.db.Product.findAll({
        where: {
            id: {
                $ne: null
            }
        }
    }).then(function (products) {
        res.render('public/index', {
            title: 'Viva el Carro',
            year: new Date().getFullYear(),
            products: products
        });
    });
}
exports.index = index;
function about(req, res) {
    res.render('public/about', { title: 'About', year: new Date().getFullYear(), message: 'Your application description page' });
}
exports.about = about;
function contact(req, res) {
    res.render('public/contact', { title: 'Contact', year: new Date().getFullYear(), message: 'Your contact page' });
}
exports.contact = contact;
//# sourceMappingURL=HomeController.js.map