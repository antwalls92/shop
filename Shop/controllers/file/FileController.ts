﻿

export function upload_file(req, res, destiny, callbacks) {
    var fs = require('fs');
    if (req.file != null) {
        fs.readFile(req.file.path, function (err, data) {
            var newPath = 'public/'+ destiny + req.file.originalname;
            var tmp_path = req.file.path;

            var src = fs.createReadStream(tmp_path);
            var dest = fs.createWriteStream(newPath);
            src.pipe(dest);

            src.on('end', function () {
                callbacks.success();
            });
        })
    } else {
        callbacks.error();
    }

}