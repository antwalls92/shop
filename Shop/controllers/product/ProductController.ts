﻿import * as express from "express";
import {db} from "../../app"
import {upload_file} from "../file/FileController"

export function index(req: express.Request, res: express.Response) {
    var products = db.Product.getAll();
    res.json(products);
}
 export function getAll() {
     var i = 0;
     return db.Product.findAll({
         where: {
             id: {
                 $ne: null
             }
         }
     })
}
 export function create(req: express.Request, res: express.Response) {
     res.render('product/create', { layout: "../admin/layout.jade" });
 }

 export function edit(req: express.Request, res: express.Response) {
     var id = req.params.id;
     var product = db.Product.findById(id).then(function (product) {
         res.render('product/edit', { product: product });
     });
 }
export function erase(req: express.Request, res: express.Response) {
    var id = req.params.id;
    var product = db.Product.findById(id).then(function (product) {
        product.destroy()
            .then(function() {
                res.redirect('/admin');
            });

    });
}
export function saveProduct(req: express.Request, res: express.Response) {
    var fs = require('fs');
    var newProd = {
        title: req.body.title,
        description: req.body.description,
        price: req.body.price,
        type: 1,
        id: req.body.id
    }

    var save_and_redirect_adminindex = function (newProd) {
        db.Product.upsert(newProd)
            .then(function (product) {
                res.redirect("/admin")
            })
    }
    upload_file(req, res, "/images/", {
        success: function () {
            newProd["image"] = "/images/" + req.file.originalname;
            save_and_redirect_adminindex(newProd)
        },
        error: function () {
            save_and_redirect_adminindex(newProd)
        }
    })
 };
     
     
  