"use strict";
var app_1 = require("../../app");
var FileController_1 = require("../file/FileController");
function index(req, res) {
    var products = app_1.db.Product.getAll();
    res.json(products);
}
exports.index = index;
function getAll() {
    var i = 0;
    return app_1.db.Product.findAll({
        where: {
            id: {
                $ne: null
            }
        }
    });
}
exports.getAll = getAll;
function create(req, res) {
    res.render('product/create', { layout: "../admin/layout.jade" });
}
exports.create = create;
function edit(req, res) {
    var id = req.params.id;
    var product = app_1.db.Product.findById(id).then(function (product) {
        res.render('product/edit', { product: product });
    });
}
exports.edit = edit;
function erase(req, res) {
    var id = req.params.id;
    var product = app_1.db.Product.findById(id).then(function (product) {
        product.destroy()
            .then(function () {
            res.redirect('/admin');
        });
    });
}
exports.erase = erase;
function saveProduct(req, res) {
    var fs = require('fs');
    var newProd = {
        title: req.body.title,
        description: req.body.description,
        price: req.body.price,
        type: 1,
        id: req.body.id
    };
    var save_and_redirect_adminindex = function (newProd) {
        app_1.db.Product.upsert(newProd)
            .then(function (product) {
            res.redirect("/admin");
        });
    };
    FileController_1.upload_file(req, res, "/images/", {
        success: function () {
            newProd["image"] = "/images/" + req.file.originalname;
            save_and_redirect_adminindex(newProd);
        },
        error: function () {
            save_and_redirect_adminindex(newProd);
        }
    });
}
exports.saveProduct = saveProduct;
;
//# sourceMappingURL=ProductController.js.map