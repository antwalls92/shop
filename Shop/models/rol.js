'use strict';
module.exports = function(sequelize, DataTypes) {
  var Rol = sequelize.define('Rol', {
    description: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Rol;
};