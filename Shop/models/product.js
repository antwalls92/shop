'use strict';
module.exports = function(sequelize, DataTypes) {
  var Product = sequelize.define('Product', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    price: DataTypes.INTEGER,
    image: DataTypes.STRING,
    type: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
    });
    //Product.upsert = function (newProd, callback) {
    //    if (newProd.id) {
    //        var id = newProd.id;
            
    //        Product.findById(id)
    //        .then(function (prod) {
    //            if (prod) {
    //                prod.update(newProd.dataValues);
    //            } else {
    //                newProd.save();
    //            }
    //            callback();
    //        })
    //    } else {
    //        return newProd.save()
    //    }
    return Product;
};