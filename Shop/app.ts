﻿import express = require('express');
import http = require('http');
import path = require('path');  
var multer = require('multer');
var Upload = multer({ dest: 'uploads/' });
import {mainrouter} from './routes/routes';
var db = require("./models");
var session = require("client-sessions");

var app = express();

// all environments & middelwares
app.use(session({
    cookieName: "session",
    secret: "random_string_goes_here",
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
}));

app.use((req, res, next) => {
    if (req.session && req.session.user) {
        db.User.find({
            where: {
                 username: req.session.user.username
            }
    }).then( user => {
            if (user) {
                req.user = user;
                delete req.user.password; // delete the password from the session
                req.session.user = user;  //refresh the session value
                res.locals.user = user;
            }
            // finishing processing the middleware and run the route
            next();
        });
    } else {
        next();
    }
});

export function requireLogin(req, res, next) {
    if (!req.user) {
        res.redirect("/admin/login");
    } else {
        next();
    }
};

export function logout(req, res) {
    req.session.reset();
    res.redirect("/");
}

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);



import stylus = require('stylus');
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.locals.basedir = path.join(__dirname, 'views');

// development only
if ('development' === app.get('env')) {
    app.use(express.errorHandler());
}

mainrouter();

http.createServer(app).listen(app.get('port'), () => {
    console.log('Express server listening on port ' + app.get('port'));
});

export {app}
export {Upload}
export {db}